const path = require('path')
const fs = require('fs')
const webpack = require('webpack')
const ejs = require('ejs');

const HTMLWebpackPlugin = require('html-webpack-plugin')
const { CleanWebpackPlugin } = require('clean-webpack-plugin')
const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const CopyPlugin = require('copy-webpack-plugin')

// Our function that generates our htsml plugins
const generateHtmlPlugins = (templateDir) => {
  // Read files in template directory
  const templateFiles = fs.readdirSync(path.resolve(__dirname, templateDir))
  return templateFiles.map(item => {
    // Split names and extension
    const parts = item.split('.')
    const name = parts[0]
    const extension = parts[1]
    // Create new HTMLWebpackPlugin with options
    return new HTMLWebpackPlugin({
      filename: `${name}.html`,
      inject: true,
      template: path.resolve(__dirname, `${templateDir}/${name}.${extension}`)
    })
  })
}

const transformHtml = env => content => ejs.render(content.toString(), { env })

// Call our function on our views directory.
// const htmlPlugins = generateHtmlPlugins('./src/pages')
const VueLoaderPlugin = require('vue-loader/lib/plugin')

module.exports = env => ({
  mode: env,
  entry: {
    index: './src/index.js',
  },

  output: {
    path: path.resolve('dist')
  },

  resolve: {
    extensions: ['.js', '.vue', '.json'],
    alias: {
      'c': path.resolve('src/components'),
      'assets': path.resolve('src/assets')
    }
  },

  module: {
    rules: [
      {
        test: /\.m?js$/,
        exclude: /(node_modules|bower_components)/,
        use: {
          loader: 'babel-loader'
        }
      },
      {
        test: /\.vue$/,
        loader: 'vue-loader'
      },
      {
        test: /\.pug$/,
        oneOf: [
          // this applies to `<template lang="pug">` in Vue components
          {
            resourceQuery: /^\?vue/,
            use: ['pug-plain-loader']
          },
          // this applies to pug imports inside JavaScript
          // {
          //   use: [
          //     'html-loader',
          //     {
          //       loader: 'pug-html-loader',
          //       options: {
          //         pretty: true,
          //         // data
          //       }
          //     }
          //   ]
          // }
        ]
      },
      // {
      //   test: /\.pug$/,
      //   oneOf: [
      //     // this applies to `<template lang="pug">` in Vue components
      //     {
      //       resourceQuery: /^\?pug/,
      //       exclude: /(node_modules|bower_components)/,
      //       use: [
      //         'html-loader',
      //         {
      //           loader: 'pug-html-loader',
      //           options: {
      //             pretty: true,
      //             // data
      //           }
      //         }
      //       ]
      //     }
      //   ]

      // },
      {
        test: /\.styl(us)?$/,
        use: [
          {
            loader: MiniCssExtractPlugin.loader,
            options: {
              hmr: env === 'development'
            },
          },
          'css-loader',
          'postcss-loader',
          'stylus-loader',
        ]
      },
      {
        test: /\.(png|svg|jpg|gif)$/,
        use: [
          {
            loader: 'file-loader',
            options: {
              // name: '[name].[ext]',
              esModule: false,
              outputPath: 'assets/'
            }
          }
        ]
      }
    ]
  },
  plugins: [
    new CleanWebpackPlugin(),

    new webpack.LoaderOptionsPlugin({
      options: {
        context: __dirname,
        stylus: {
          use: [require('rupture')()],
          preferPathResolver: 'webpack',
          options: ['resolve url']
        }
      }
    }),

    new CopyPlugin([
      { from: 'src/index.html', to: 'index.html', transform: transformHtml(env) },
      // {
      //   from: `icons/icon_128.png`,
      //   to: 'assets/icon_128.png',
      //   context: 'src/assets/'
      // }
    ]),

    new VueLoaderPlugin(),

    // new HtmlWebpackPlugin({
    //   template: './src/index.pug',
    //   minify: false
    // }),

    new MiniCssExtractPlugin({
      // Options similar to the same options in webpackOptions.output
      // all options are optional
      filename: 'index.css',
      chunkFilename: '[id].css',
      ignoreOrder: false, // Enable to remove warnings about conflicting order
    }),
  ]
  // .concat(htmlPlugins)
})
