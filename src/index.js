// require('../css/index.styl')

// const btc = require('bitcoinjs-lib')

// // const Http = new XMLHttpRequest();
// // const url='https://blockchain.info/balance?cors=true&active=';

// document.addEventListener('DOMContentLoaded', () => {
//   const form = document.querySelector('form.input')
//   const textarea = form.querySelector('textarea')

//   textarea.addEventListener('input', () => {
//     const { value } = textarea
//     textarea.dataset.type = /^[a-zA-Z]+$/.test(value) ?
//       'alpha' :
//       /^[0-1]{1,}$/.test(value) ?
//         'binary' :
//         /^\d+$/.test(value) ?
//           'numeric' :
//           /^[0-9a-zA-Z]+$/.test(value) ?
//             'alphanumeric' :
//             'empty'

//     console.log(value, textarea.dataset.type)
//   })

// })

// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue/dist/vue'
import App from './pages/index.vue'

Vue.config.productionTip = false
Vue.config.devtools = true
// Vue.config.devtools = process.env === 'development'

/* eslint-disable no-new */
new Vue({
  el: '#v',
  components: { App },
  render: h => h(App)
})
