const btc = require('bitcoinjs-lib')
const base =  '0'.repeat(64)

function changeEndianness (string) {
  const result = [];
  let len = string.length - 2;
  while (len >= 0) {
    result.push(string.substr(len, 2));
    len -= 2;
  }
  return result.join('');
}

export default function keypairFromHex (hex, compressed = true, bigEndian = false ) {
  let _hex = base.substr(0, 64 - hex.length) + hex

  if (bigEndian)
    _hex = changeEndianness(_hex)

  const hash = Buffer.from(_hex, 'hex')

  const keyPair = btc.ECPair.fromPrivateKey(hash, { compressed })
  const address = btc.payments.p2pkh({ pubkey: keyPair.publicKey }, {}).address
  const privateKey = keyPair.toWIF()

  return {
    address,
    privateKey
  }
}
